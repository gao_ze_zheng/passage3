#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include "sem_com.c"

#define DELAY_TIME 3
int main() {
pid_t pid;
int sem_id;
key_t sem_key;
sem_key=ftok(".",'a');
sem_id=semget(sem_key,1,0666|IPC_CREAT);
init_sem(sem_id,1);

 if ((pid=fork())<0) {
    perror("Fork error!\n");
     exit(1);
 } else if (pid==0) {
    sem_p(sem_id); 
     printf("Child running...\n");
     sleep(DELAY_TIME);
     printf("Child %d,returned value:%d.\n",getpid(),pid);
     sem_v(sem_id);
     exit(0);
 } else {
     sem_p(sem_id); 
    printf("Parent running!\n");
   sleep(DELAY_TIME);
    printf("Parent %d,returned value:%d.\n",getpid(),pid);
   sem_v(sem_id); 
     waitpid(pid,0,0);
     del_sem(sem_id);
   exit(0);
}
}