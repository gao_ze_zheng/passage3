#ifndef __FILE_H__
#define __FILE_H__


#define NPROC   9

#define SSIZE 1024

// PROC status

#define FREE    0

#define READY   1

#define SLEEP   2

#define ZOMBIE  3

typedef struct proc{

struct proc *next;

int *ksp;

int pid;

int status;

int priority;

int  kstack [SSIZE];

}PROC;


#endif