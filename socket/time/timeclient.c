
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<strings.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
 
#define PORT 1234   /*listen port*/
 
int main ( int argc, char *argv[])
{
 	int sockfd, num;
	struct hostent *he;
 	struct sockaddr_in server;
 
	time_t t;
 	if(argc!=2)
 	{
  		printf("usage %s<ip address>\n",argv[0]);
 		return 0;
	}
	 /*get ip address*/
 	if((he = gethostbyname(argv[1])) == NULL)
 	{
  		printf("gethostbyname error\n");
                return 0;
 
 	}
 	if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
 	{
  		printf("socket() error \n");
		return 0;
 	}
  	bzero(&server, sizeof(server));
 	server.sin_family = AF_INET;
 	server.sin_port = htons(PORT);
 	server.sin_addr = *((struct in_addr *)he->h_addr);
 
   	if(connect(sockfd, (struct sockaddr *)&server, sizeof(server)) == -1)
 	{
  		printf("connetc() error\n");
		return 0;
 	}
	recv(sockfd,(void *)&t,sizeof(time_t),0);
	printf("Time is %s\n",ctime(&t));
}
