#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<string.h>
#include<errno.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<iostream>
#include<pthread.h>
using namespace std;

#define SERVERIP "127.0.0.1"
#define SERVERPORT 13000
#define MAXBUFFER 256

pthread_t ntid;
int connfd;
struct sockaddr_in clientaddr;


void *printContext(void *arg)
{

    char ip[40]={0};
    char readBuf[MAXBUFFER]={0};
    int ret;
    pthread_detach(ntid);
    int pconnfd=connfd;
    printf("%s\n",inet_ntop(AF_INET,&clientaddr.sin_addr,ip,sizeof(ip)));
    cout<<"connectd to the servce"<<endl;

    while(ret=read(pconnfd,readBuf,MAXBUFFER))
    {
        write(pconnfd,readBuf,MAXBUFFER);
        printf("%s\n",readBuf);
        bzero(readBuf,MAXBUFFER);

    }
    if(ret==0)
    {
        printf("the connection of client is close!\n");

    }else
    {
        printf("read error:%s\n",strerror(errno));
    }
    pthread_exit(0);
}

int main(int argc,char** argv)
{
    socklen_t len;
    int serverFd,ret;
    struct sockaddr_in serveraddr;


    serverFd=socket(AF_INET,SOCK_STREAM,0);
    if(serverFd<0)
    {
        printf("socket error:%s\n",strerror(errno));
        exit(-1);
    }

    bzero(&serveraddr,sizeof(serveraddr));
    serveraddr.sin_family=AF_INET;
    serveraddr.sin_port=htons(SERVERPORT);
    inet_pton(AF_INET,SERVERIP,&serveraddr.sin_addr);

    ret=bind(serverFd,(struct sockaddr *)&serveraddr,sizeof(serveraddr));
    if(ret!=0)
    {
        close(serverFd);
        printf("bind error:%\n",strerror(errno));
        exit(-1);
    }

    ret=listen(serverFd,5);
    if(ret!=0)
    {
        close(serverFd);
        printf("listen error:%s\n",strerror(errno));
        exit(-1);
    }

    len=sizeof(clientaddr);
    bzero(&clientaddr,sizeof(clientaddr));
    while(1)
    {
        connfd=accept(serverFd,(struct sockaddr *) &clientaddr,&len);
        if(connfd<0)
        {
            printf("accept error:%s\n",strerror(errno));
            continue;
        }

        int err;
        err=pthread_create(&ntid,NULL,printContext,NULL);
        if(err!=0)
        {
            cout<<"can't create pthread"<<endl;
        }
        //close(connfd);
    }
    close(serverFd);
    return 0;
}