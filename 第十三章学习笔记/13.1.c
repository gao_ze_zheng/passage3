#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
 
#define MAXLINE 256
#define PORT 7777
void sys_err(char *msg){
    perror(msg);
    exit(-1);
}
int main(int argc , char **argv){
 
 
    int sockFd,n;
    char recvLine[MAXLINE];
    struct sockaddr_in servAddr;
 
    if (argc != 2) {
        sys_err("usage: a.out <IPaddress>");
    }
 
    sockFd=socket(AF_INET,SOCK_STREAM,0);
 
 
    memset(&servAddr,0,sizeof(servAddr));
 
    servAddr.sin_family = AF_INET;
    servAddr.sin_port = htons(PORT);
    if (inet_pton(AF_INET,argv[1],&servAddr.sin_addr) <= 0) {
 
        sys_err("inet_pton error");
    }
 
    connect(sockFd,(struct sockaddr *)&servAddr,sizeof(servAddr));
 
 
    while((n=read(sockFd,recvLine,MAXLINE)) >0 ){
        recvLine[n] = '\0';
        if(fputs(recvLine,stdout) == EOF){
            sys_err("fputs error");
        }
    }
    if(n <0){
        sys_err("read error");
    }
    return 0;
}
